$(function () {
    var prizeName = getUrlParam('prizeName');
    if (prizeName == '谢谢参与') {
        $('#kudo_text1').text('谢谢参与');
    } else {
        $('#kudo_text2').text(prizeName);
    }
    $('#fomclick').click(function() {
        alert('开启FOM...');
    });
});
//获取url参数
function getUrlParam(name) {
    //构造一个含有目标参数的正则表达式对象 
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    //匹配目标参数 
    var r = window.location.search.substr(1).match(reg);
    //alert(r); 
    //返回参数值 
    if (r != null) return decodeURI(r[2]);
    return null;
}