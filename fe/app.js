//DOM加载完成后就执行之
$(function() {
    var hikaliv = {};
    hikaliv.AppBridge = new AppBridgeFunc();
    hikaliv.Tools = new ToolsFunc(hikaliv.AppBridge);
    hikaliv.User = new UserFunc(hikaliv.Tools);
    hikaliv.Data = new DataFunc(hikaliv.User, hikaliv.Tools);
    window.hikaliv = hikaliv;
    //填充活动详情页面的相关信息
    fillDetail();
    //用户信息初始化
    hikaliv.User.init();
    //点击活动详情按钮时显示活动详情页面
    $('#showdetail').click(showDetail);
    //点击活动详情页面的返回按钮时隐藏活动详情页面
    $('#returnP').click(hideDetail);
});

//页面所有元素加载完毕之后执行
window.onload = function() {
    $('#pan').click(function() {
        if (window.hikaliv.Data.isRunning) {
            return;
        }
        //while (document.readyState != 'complete');
        setTimeout(function() {
            /*
            if (!window.WebViewJavascriptBridge) {
                var conret = confirm('下载视淘app，参与现场互动，抽大奖');
                if (conret) {
                    window.open('http://www.shitaoapp.com');
                }
            } else {
                var User = window.hikaliv.User;
                var Tools = window.hikaliv.Tools;
                if (!User.info.isLogin) {
                    Tools.bridge('User.Get', '', function(data, bridge) {
                        data = Tools.promisejson(data);
                        if (!!data && !!data.uid) {
                            User.fill(data);
                            lottery();
                        } else {
                            bridge.callHandler('User.Login', '', function(data) {
                                data = Tools.promisejson(data);
                                if (!!data && !!data.uid) {
                                    User.fill(data);
                                    lottery();
                                }
                            });
                        }
                    });
                } else {
                    lottery();
                }
            }
            */

            var User = window.hikaliv.User;
            var Tools = window.hikaliv.Tools;
            if (!User.info.isLogin) {
                Tools.bridge('User.Get', '', function(data, bridge) {
                    data = Tools.promisejson(data);
                    if (!!data && !!data.uid) {
                        User.fill(data);
                        lottery();
                    } else {
                        bridge.callHandler('User.Login', '', function(data) {
                            data = Tools.promisejson(data);
                            if (!!data && !!data.uid) {
                                User.fill(data);
                                lottery();
                            }
                        });
                    }
                });
            } else {
                lottery();
            }
        }, 100);
    });
};

//填充活动详情的信息
function fillDetail() {
    var hikaliv = window.hikaliv;
    var Data = hikaliv.Data;
    var key = setInterval(function() {
        if (Data.isDetailReady) {
            clearInterval(key);

            //加载奖品图
            $('#panbg').css('background-image', Data.panpic);

            $('#detailtime').text(Data.duraion);
            $('#detaildis').text(Data.content);
            $('#detailprize').text(Data.description);
        }
    }, 10);
}

//显示活动详情页面
function showDetail() {
    //alert('活动详情');
    $('#panBox').css('display', 'none');
    $('#showdetail').css('visibility', 'hidden');
    $('#masklayer').css('display', 'block');
    $('#detailDiv').css('display', 'block');
}

//隐藏活动详情页面
function hideDetail() {
    $('#panBox').css('display', 'block');
    $('#showdetail').css('visibility', 'visible');
    $('#masklayer').css('display', 'none');
    $('#detailDiv').css('display', 'none');
}

//WebViewJavascriptBridge
function AppBridgeFunc() {
    this.connectWebViewJavascriptBridge = function(callback) {
        if (window.WebViewJavascriptBridge) {
            callback(WebViewJavascriptBridge);
        } else {
            document.addEventListener('WebViewJavascriptBridgeReady', function() {
                callback(WebViewJavascriptBridge);
            }, false);
        }
    };
    this.connectWebViewJavascriptBridge(function(bridge) {
        bridge.init(function(message, responseCallback) {
            if (responseCallback) {
                responseCallback('this is seetao app javascript bridge');
            }
        });
    });
}

//工具对象构造函数
function ToolsFunc(AppBridge) {
    this.getTime = function(start, end) {
        var s = new Date(start * 1000);
        var e = new Date(end * 1000);
        var str = '活动时间：' + (s.getMonth() + 1) + '月' + s.getDate() + '日 - ' + (e.getMonth() + 1) + '月' + e.getDate() + '日';
        return str;
    };
    this.bridge = function(name, paras, callback) {
        AppBridge.connectWebViewJavascriptBridge(function(bridge) {
            bridge.callHandler(name, paras, function(data) {
                callback(data, bridge);
            });
        });
    };
    this.prompt = function(info) {
        AppBridge.connectWebViewJavascriptBridge(function(bridge) {
            bridge.callHandler('Prompt.Msg', info);
        });
    };
    this.promisejson = function(data) {
        if (typeof(data) === 'string') {
            try {
              data = JSON.parse(data);
            } catch (e) {}
        }
        return data;
    };
    this.checkjson = function(data) {
        var str = '';
        for (var key in data) {
            str += key + ' ' + data[key] + '\n';
        }
        alert(str);
    };
}

//用户对象构造函数
function UserFunc(Tools) {
    this.locsearch = window.location.search;
    this.info = {};
    this.info.token = '';
    this.info.uid = '';
    this.info.nickname = '';
    this.info.isLogin = false;
    this.info.imageUrl = '';
    var that = this;
    /*
    this.fill = function(data) {
        that.info.uid = data.uid;
        that.info.token = data.token;
        that.info.nickname = data.nickname;
        that.info.isLogin = true;
    };
    this.init = function() {
        Tools.bridge('User.Get', '', function(data) {
            data = Tools.promisejson(data);
            if (!!data && !!data.uid) {
                that.fill(data);
            }
        });
    };
    */
    this.fill = function(data) {
        this.info.uid = 'weibo:1620653272';
        this.info.isLogin = true;
        this.info.token = 'd04ffccad51827f743ac0180cd2289a7';
        this.info.nickname = 'thinkcc731';
    };
    this.init = function() {
        this.info.uid = 'weibo:1620653272';
        this.info.isLogin = true;
        this.info.nickname = 'thinkcc731';
        this.info.token = 'd04ffccad51827f743ac0180cd2289a7';
    };

}

//数据对象构造函数
function DataFunc(User, Tools) {
    this.sharepackage = {
        'title': '幸运大抽奖，玩的就是手气！我已经用尽全身运气，谁敢来跟我比比！',
        'image': 'http://shitaoapp.b0.upaiyun.com/other/1852B13F-77D7-3877-CDDE-45EAF9EE4A91.png_540',
        'content': '幸运大抽奖，玩的就是手气！看电视的时间我可没闲着，视淘带我抽奖带我赢~~下载视淘app捞尽全部奖品。@视淘 凭我的手气必须全都中呀，快速下载http://www.shitaoapp.com',
        'url': window.location.toString()
    };
    this.multiples = 1;
    var that = this;
    $.ajax({
        url: '/lotteryDetailQuery' + User.locsearch,
        type: 'GET',
        success: function(data) {
            data = Tools.promisejson(data);
            that.content = data.content;
            that.description = data.description;
            that.duraion = Tools.getTime(data.startTime, data.endTime);
            that.panpic = 'url(' + data.mainPic + ')';
            that.bgimg = 'url(' + data.background + ')';
            that.slogan = 'url(' + data.slogan + ')';
            that.operation = +data.operation;
            that.isDetailReady = true;
        }
    });
    $.ajax({
        url: '/lotteryWinnerQuery' + User.locsearch,
        type: 'GET',
        success: function(data) {
            data = Tools.promisejson(data);
            var str = '';
            for (var i = 0, l = data.length; i < l; i++) {
                str += '恭喜' + data[i].nickname + '获得奖品' + data[i].title + '!  ';
            }
            if (str === '') {
                str = '尚无中奖者~兴许你就是第一个中奖者噢！';
            }
            that.luckyer = str;
            that.isWinnerReady = true;
        }
    });
}

//点击抽奖之后进行的操作
function lottery() {
    var hikaliv = window.hikaliv;
    var Data = hikaliv.Data;
    var User = hikaliv.User;
    var Tools = hikaliv.Tools;
    Data.isRunning = true;

    var count = 1;
    var time = 0; //转了多少毫秒
    var maxTime = 10000; //最多转10000毫秒
    var delayTime = 3000; //由于动画延时的原因，抽奖失败的提示也应该延时才行
    var key = setInterval(function() {
        if (time < maxTime) {
            rotatePan(count*360, 1);
            count++;
        } else {
            clearInterval(key);
            setTimeout(function() {
                showInfo('~亲，抽奖失败了，请检查网络后再试试吧！');
            }, delayTime);
        }
        time += 50;
    }, 50); 

    
    $.ajax({
        url: '/lotteryDrawQuery' + User.locsearch + '&uid=' + User.info.uid + '&cookie=' + User.info.token,
        type: 'GET',
        success: function(data) {
            var isBingo = false;
            var can = false;
            User.info.imageUrl = '';
            data = Tools.promisejson(data);

            clearInterval(key);
            if (data && typeof(data.times) != 'undefined') {
                can = true;
                User.info.trueid = data.trueId;
                if (data.id !== -1 && data.id !== null) {
                    User.info.imageUrl = data.imageUrl;
                    isBingo = true;
                }
            } else if (data && data.errCode) {
                //showInfo(data.errCode);
                switch (data.errCode) {
                    case 60003:
                        Tools.prompt('抽奖活动尚未开始');
                        break;
                    case 60001:
                        Tools.prompt('抽奖活动已结束');
                        break;
                    case 70008:
                        Tools.prompt('你已经没有抽奖机会了噢～');
                        break;
                    case 70009:
                        Tools.prompt('你已经不能再抽奖了噢~');
                        break;
                    default:
                        Tools.prompt(data.errCode);
                        break;
                };
            } else {
                showInfo('网络中断');
                Tools.prompt('网络中断');
            }
            if (can) {
                var angleStart = data.angleStart;
                var angleEnd = data.angleEnd;
                var angle = 0.5 * (Math.abs(angleEnd - angleStart)) + angleStart;
                
                rotatePan(count*1800 + angle, 1);
                Data.multiples++;
                setTimeout(function() {
                    
                    if (isBingo) { //如果中奖了
                        if (Data.operation && Data.operation === 2) {
                            //跳转到中奖页面
                            //showInfo(User.info.nickname + data.title);
                            window.location.replace('/win?prizeName=' + data.title);
                        }
                    } else { //如果没有中奖
                        showInfo('啊，一不小心，没有中奖！');
                        //window.location.replace('/win?prizeName=' + data.title);
                    }
                    Data.isRunning = false;
                }, 3200);
            } else {
                Data.isRunning = false;
            }
        }
    });
}

//选择抽奖转盘,angle是角度,time是几秒转完
function rotatePan(angle, time) {
    $('#pan').css({
        '-webkit-transform': 'rotate(' + angle + 'deg)',
        'transform': 'rotate(' + angle + 'deg)',
        '-moz-transform': 'rotate(' + angle + 'deg)',
        '-o-transform': 'rotate(' + angle + 'deg)',
        '-webkit-transition': '-webkit-transform ' + time + 's ease-in-out',
        '-moz-transition': '-moz-transform ' + time + 's ease-in-out',
        '-o-transition': '-o-transform ' + time + 's ease-in-out',
        'transition': 'transform 3s ease-in-out'
    });
}
//给出提示
function showInfo(message) {
    alert(message);
}


