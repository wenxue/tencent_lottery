var express = require('express');
var router = express.Router();
var path = require('path');
var util = require('util');
var url = require('url');
var http = require('http');

var cacheTime = 0;
router.get('/lottery', function(req, res, next) {
    
    res.sendFile('index.html', {
        root: path.join(__dirname, '../public'),
        maxAge: cacheTime
    });
});
router.get('/win', function(req, res, next) {

    res.sendFile('win.html', {
        root: path.join(__dirname, '../public'),
        maxAge: cacheTime
    });
}); 
router.get('/test', function(req, res, next) {

    res.sendFile('test.html', {
        root: path.join(__dirname, '../public'),
        maxAge: cacheTime
    });
}); 

router.get('/lotteryDetailQuery', handler('/lottery_detail.php'));
router.get('/lotteryWinnerQuery', handler('/lottery_winners.php'));
router.get('/lotteryDrawQuery', handler('/lottery_draw.php'));

//产生可配置的中间件
var urlPart = 'http://172.16.3.11:8080/api2.0';
function handler(logicUrl) {

    return function(req, res, next) {
        var search = '?';
        for (var key in req.query) {
            search += key + '=' + req.query[key] + '&'
        }
        if (search.length > 1) {
            search += 'pf=webshare&did=0';
            var url = urlPart + logicUrl + search;
            console.log(url);
            httpGetData(url, function(data) {
                res.write(data);
                res.end();
            });
        }

    };
}

//向数据服务器请求数据
function httpGetData(url, callback) {
    http.get(url, function(res) {
        var source = '';
        res.on('data', function(data) {
            source += data;
        });
        res.on('end', function() {
            console.log(source);
            callback(source);
        });
    }).on('error', function() {
        console.log('fuck,json request error.');
        callback('error');
    });
}

module.exports = router;
