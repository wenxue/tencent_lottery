//引入gulp
var gulp = require('gulp');
//引入组件
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minify = require('gulp-minify-css');

//输出jquery文件
gulp.task('zepto', function() {
    var files = [
        'bower_components/zepto/zepto.min.js'
    ];
    gulp.src(files)
        .pipe(gulp.dest('public/javascripts'));

});

//输出html文件
gulp.task('html', function() {
    gulp.src('fe/*.html')
        .pipe(gulp.dest('public'));
});

//压缩js文件
gulp.task('js', function() {
    gulp.src('fe/app.js')
        .pipe(jshint())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('public/javascripts'))
        .pipe(uglify())
        .pipe(gulp.dest('public/javascripts'));
    gulp.src('fe/win.js')
        .pipe(jshint())
        .pipe(concat('win.min.js'))
        .pipe(gulp.dest('public/javascripts'))
        .pipe(uglify())
        .pipe(gulp.dest('public/javascripts'));
});

//压缩css文件
gulp.task('css', function() {
    gulp.src('fe/*.css')
        .pipe(minify())
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('public/stylesheets'));
});

//默认任务
gulp.task('default', ['zepto', 'html', 'js', 'css'], function() {
    console.log('gulp finished.');
});

